<?php
namespace DekodiRest\Controllers;

use Plenty\Modules\Account\Address\Contracts\AddressRepositoryContract;
use Plenty\Modules\Account\Contact\Contracts\ContactRepositoryContract;
use Plenty\Modules\Account\Contracts\AccountRepositoryContract;
use Plenty\Modules\Document\Contracts\DocumentRepositoryContract;
use Plenty\Modules\Document\Models\Document;
use Plenty\Modules\Item\Item\Contracts\ItemRepositoryContract;
use Plenty\Modules\Item\Variation\Contracts\VariationRepositoryContract;
use Plenty\Modules\Item\VariationBarcode\Contracts\VariationBarcodeRepositoryContract;
use Plenty\Modules\Item\VariationMarketIdentNumber\Contracts\VariationMarketIdentNumberRepositoryContract;
use Plenty\Modules\Item\VariationSku\Contracts\VariationSkuRepositoryContract;
use Plenty\Modules\Order\Address\Contracts\OrderAddressRepositoryContract;
use Plenty\Modules\Order\Contracts\OrderRepositoryContract;
use Plenty\Modules\Order\Date\Models\OrderDateType;
use Plenty\Modules\Order\Models\Order;
use Plenty\Modules\Order\RelationReference\Models\OrderRelationReference;
use Plenty\Modules\Order\Property\Models\OrderPropertyType;
use Plenty\Modules\Order\Shipping\Information\Contracts\ShippingInformationRepositoryContract;
use Plenty\Modules\Order\Shipping\Package\Contracts\OrderShippingPackageRepositoryContract;
use Plenty\Modules\Payment\Contracts\PaymentRepositoryContract;
use Plenty\Modules\StockManagement\Warehouse\Contracts\WarehouseRepositoryContract;
use Plenty\Plugin\Controller;
use Plenty\Plugin\Http\Request;
use Plenty\Plugin\Http\Response;

/**
 * Class ContentController
 * @package HelloWorld\Controllers
 */
class OrdersController extends Controller
{
    /**
     * @var OrderRepositoryContract
     */
    private $orderRepo;

    /**
     * @var ContactRepositoryContract
     */
    private $contactRepo;

    /**
     * @var AccountRepositoryContract
     */
    private $accountRepo;

    /***
     * @var WarehouseRepositoryContract
     */
    private $warehouseRepo;

    /**
     * @var AddressRepositoryContract
     */
    private $addressRepo;

    /**
     * @var VariationRepositoryContract
     */
    private $variationRepo;

    /**
     * @var VariationMarketIdentNumberRepositoryContract
     */
    private $marketItemNumbersRepo;

    /**
     * @var PaymentRepositoryContract
     */
    private $paymentRepo;

    /**
     * @var ItemRepositoryContract
     */
    private $itemRepo;

    /**
     * @var VariationSkuRepositoryContract
     */
    private $skuRepo;

    /**
     * @var ShippingInformationRepositoryContract
     */
    private $shippingInformationRepo;

    /**
     * @var VariationBarcodeRepositoryContract
     */
    private $variationBarcodeRepo;

    /**
     * @var OrderShippingPackageRepositoryContract
     */
    private $orderShippingPackageRepo;

    /**
     * Get all orders.
     *
     * @param Response $response
     * @param Request $request
     * @param OrderRepositoryContract $orderRepo
     * @param ContactRepositoryContract $contactRepo
     * @param AccountRepositoryContract $accountRepo
     * @param WarehouseRepositoryContract $warehouseRepo
     * @param OrderAddressRepositoryContract $addressRepo
     *
     * @return string
     */
    public function all(Response $response, Request $request, OrderRepositoryContract $orderRepo,
                        ContactRepositoryContract $contactRepo, AccountRepositoryContract $accountRepo,
                        WarehouseRepositoryContract $warehouseRepo, OrderAddressRepositoryContract $addressRepo,
                        VariationRepositoryContract $variationRepo, VariationMarketIdentNumberRepositoryContract $marketItemNumbersRepo,
                        PaymentRepositoryContract $paymentRepo, ItemRepositoryContract $itemRepo, DocumentRepositoryContract $documentRepo,
                        VariationSkuRepositoryContract $skuRepo, ShippingInformationRepositoryContract $shippingInformationRepo,
                        VariationBarcodeRepositoryContract $variationBarcodeRepo, OrderShippingPackageRepositoryContract $orderShippingPackageRepo)
    {
        $this->orderRepo = $orderRepo;
        $this->contactRepo = $contactRepo;
        $this->accountRepo = $accountRepo;
        $this->warehouseRepo = $warehouseRepo;
        $this->addressRepo = $addressRepo;
        $this->variationRepo = $variationRepo;
        $this->marketItemNumbersRepo = $marketItemNumbersRepo;
        $this->paymentRepo = $paymentRepo;
        $this->itemRepo = $itemRepo;
        $this->skuRepo = $skuRepo;
        $this->shippingInformationRepo = $shippingInformationRepo;
        $this->variationBarcodeRepo = $variationBarcodeRepo;
        $this->orderShippingPackageRepo = $orderShippingPackageRepo;

        $withDocumentContents = $request->get('documentContents', 0) == 1;

        if ($request->get('createdAtFrom', null) !== null) {
            $documentRepo->setFilters([
                'createdAtFrom' => $request->get('createdAtFrom', null),
                'createdAtTo' => $request->get('createdAtTo', null),
                'withContent' => $withDocumentContents
            ]);
        }

        /** @var Order[] $orders */
        $documents = $documentRepo->find($request->get('page', 1), $request->get('ipp', 10), ['id', 'type', 'number', 'numberWithPrefix', 'path', 'userId', 'source', 'displayDate', 'createdAt', 'updatedAt'], ['references']
        )->getResult();

        $resultDocuments = [];
        foreach ($documents as &$document) {
            $documentOrders = [];
            foreach($document['references'] as $reference) {
                if ($reference['type'] == 'order') {
                    $order = $this->orderRepo->findOrderById($reference['value'], ["addresses", "events", "dates", "relation", "references", "location", "payments", "comments"])->toArray();
                    $this->transformOrder($order, $documentOrders, $withDocumentContents);
                }
            }
            $document['orders'] = $documentOrders;
            $resultDocuments[] = $document;
        }

        if (!$withDocumentContents) {
            return $response->json( ['data' => $resultDocuments ]);
        } else {
            $documents = [];
            foreach ($resultDocuments as $resultDocument) {
                $index = $resultDocument['type'] . '_' . $resultDocument['id'];
                if (!isset($documents[$index])) {
                    $documents[$index] = $resultDocument['content'];
                }

                foreach ($resultDocument['originOrderDocuments'] as $originOrderDocument) {
                    $index = $originOrderDocument['type'] . '_' . $originOrderDocument['id'];
                    if (!isset($documents[$index])) {
                        $documents[$index] = $originOrderDocument['content'];
                    }
                }

                foreach ($resultDocument['referenceOrderDocuments'] as $referenceOrderDocument) {
                    $index = $referenceOrderDocument['type'] . '_' . $referenceOrderDocument['id'];
                    if (!isset($documents[$index])) {
                        $documents[$index] = $referenceOrderDocument['content'];
                    }
                }
            }

            return $response->json( ['data' => $documents ]);
        }
    }

    private function transformOrder($order, &$resultOrders, $withDocumentContents) {
        foreach ($resultOrders as &$resultOrder) {
            if ($resultOrder['id'] == $order['id']) {
                return;
            }
        }

        $payments = $this->paymentRepo->getPaymentsByOrderId($order['id']);
        $order['payments'] = $payments;

        $shippingInformation = $this->shippingInformationRepo->getShippingInformationByOrderId($order['id']);
        $order['shippingInformation'] = $shippingInformation;

        $packages = $this->orderShippingPackageRepo->listOrderShippingPackages($order['id'], ['id', 'orderId', 'packageId', 'weight', 'packageNumber', 'labelPath', 'packageType', 'volume']);
        $order['packages'] = $packages;

        $resultOrderItems = [];
        foreach($order['orderItems'] as $orderItem) {
            $variation = null;
            $item = null;
            $skus = [];
            $barcodes = [];
            try {
                $variation = $this->variationRepo->findById($orderItem['itemVariationId']);
                $skus = $this->skuRepo->findByVariationId($orderItem['itemVariationId']);
                $barcodes = $this->variationBarcodeRepo->findByVariationId($orderItem['itemVariationId']);
                $item = $this->itemRepo->show($variation->itemId);
            } catch(\Exception $ex) {

            }

            $orderItem['item'] = $item;
            $orderItem['variation'] = $variation;
            $orderItem['skus'] = $skus;
            $orderItem['barcodes'] = $barcodes;

            $marketItemNumbers = $this->marketItemNumbersRepo->findByVariationId($orderItem['itemVariationId']);
            $orderItem['marketItemNumbers'] = $marketItemNumbers;

            $resultOrderItems[] = $orderItem;
        }
        $order['orderItems'] = $resultOrderItems;



        $resultRelations = [];
        foreach ($order['relations'] as $relation) {
            switch ($relation['referenceType']) {
                case OrderRelationReference::REFERENCE_TYPE_CONTACT:
                    $relation['resolvedContact'] = $this->contactRepo->findContactById($relation['referenceId']);
                    break;
                case OrderRelationReference::REFERENCE_TYPE_ACCOUNT:
                    $relation['resolvedAccount'] = $this->accountRepo->findAccountById($relation['referenceId']);
                    break;
                case OrderRelationReference::REFERENCE_TYPE_WAREHOUSE:
                    $relation['resolvedWarehouse'] = $this->warehouseRepo->findById($relation['referenceId']);
                    break;
            }
            $resultRelations[] = $relation;
        }
        $order['relations'] = $resultRelations;

        $resultAddressRelations = [];
        foreach ($order['addressRelations'] as $addressRelation) {
            $addressRelation['resolvedAddress'] = $this->addressRepo->findAddressByType($order['id'], $addressRelation['typeId']);
            $resultAddressRelations[] = $addressRelation;
        }
        $order['addressRelations'] = $resultAddressRelations;

        foreach ($order['dates'] as $date) {
            switch ($date['typeId']) {
                case OrderDateType::ORDER_DELETED_DATE:
                    $order['deletedDate'] = $date['date'];
                    break;
                case OrderDateType::ORDER_ENTRY_AT:
                    $order['entryAt'] = $date['date'];
                    break;
                case OrderDateType::ORDER_PAID_ON:
                    $order['paidOn'] = $date['date'];
                    break;
                case OrderDateType::ORDER_LAST_UPDATE:
                    $order['lastUpdate'] = $date['date'];
                    break;
                case OrderDateType::ORDER_COMPLETED_ON:
                    $order['completedOn'] = $date['date'];
                    break;
                case OrderDateType::ORDER_RETURN_DATE:
                    $order['returnDate'] = $date['date'];
                    break;
                case OrderDateType::ORDER_PAYMENT_DUE_DATE:
                    $order['dueDate'] = $date['date'];
                    break;
                case OrderDateType::ORDER_ESTIMATED_SHIPPING_DATE:
                    $order['estimatedShippingDate'] = $date['date'];
                    break;
                case OrderDateType::ORDER_START_DATE:
                    $order['startDate'] = $date['date'];
                    break;
                case OrderDateType::ORDER_END_DATE:
                    $order['endDate'] = $date['date'];
                    break;
                case OrderDateType::ORDER_POSSIBLE_DELIVERY:
                    $order['possibleDelivery'] = $date['date'];
                    break;
                case OrderDateType::ORDER_MARKET_TRANSFER_DATE:
                    $order['marketTransferDate'] = $date['date'];
                    break;
            }
        }

        foreach ($order['properties'] as $property) {
            switch ($property['typeId']) {
                case OrderPropertyType::WAREHOUSE:
                    $order['warehouse'] = $this->warehouseRepo->findById($property['value']);
                    break;

                case OrderPropertyType::SHIPPING_PROFILE:
                    $order['shippingProfile'] = $property['value'];
                    break;

                case OrderPropertyType::PAYMENT_METHOD:
                    $order['paymentMethod'] = $property['value'];
                    break;

                case OrderPropertyType::PAYMENT_STATUS:
                    $order['paymentStatus'] = $property['value'];
                    break;

                case OrderPropertyType::EXTERNAL_SHIPPING_PROFILE:
                    $order['externalShippingProfile'] = $property['value'];
                    break;

                case OrderPropertyType::DOCUMENT_LANGUAGE:
                    $order['documentLanguage'] = $property['value'];
                    break;

                case OrderPropertyType::EXTERNAL_ORDER_ID:
                    $order['externalOrderId'] = $property['value'];
                    break;

                case OrderPropertyType::CUSTOMER_SIGN:
                    $order['customerSign'] = $property['value'];
                    break;

                case OrderPropertyType::DUNNING_LEVEL:
                    $order['dunningLevel'] = $property['value'];
                    break;

                case OrderPropertyType::SELLER_ACCOUNT:
                    $order['sellerAccount'] = $property['value'];
                    break;

                case OrderPropertyType::WEIGHT:
                    $order['weight'] = $property['value'];
                    break;

                case OrderPropertyType::WIDTH:
                    $order['width'] = $property['value'];
                    break;

                case OrderPropertyType::LENGTH:
                    $order['length'] = $property['value'];
                    break;

                case OrderPropertyType::HEIGHT:
                    $order['height'] = $property['value'];
                    break;

                case OrderPropertyType::FLAG:
                    $order['flag'] = $property['value'];
                    break;

                case OrderPropertyType::EXTERNAL_TOKEN_ID:
                    $order['externalTokenId'] = $property['value'];
                    break;

                case OrderPropertyType::EXTERNAL_ITEM_ID:
                    $order['externalItemId'] = $property['value'];
                    break;

                case OrderPropertyType::COUPON_CODE:
                    $order['couponCode'] = $property['value'];
                    break;

                case OrderPropertyType::COUPON_TYPE:
                    $order['couponType'] = $property['value'];
                    break;

                case OrderPropertyType::ORIGINAL_WAREHOUSE:
                    $order['originalWarehouse'] = $this->warehouseRepo->findById($property['value']);
                    break;

                case OrderPropertyType::ORIGINAL_QUANTITY:
                    $order['originalQuantity'] = $property['value'];
                    break;

                case OrderPropertyType::CATEGORY_ID:
                    $order['categoryId'] = $property['value'];
                    break;

                case OrderPropertyType::MARKET_FEE:
                    $order['marketFee'] = $property['value'];
                    break;

                case OrderPropertyType::STOCK_REVERSING:
                    $order['stockReversing'] = $property['value'];
                    break;

                case OrderPropertyType::DISPUTE_STATUS:
                    $order['disputeStatus'] = $property['value'];
                    break;

                case OrderPropertyType::NO_CHANGE_BY_CONTACT:
                    $order['noChangeByContact'] = $property['value'];
                    break;

                case OrderPropertyType::INTERVAL_TYP:
                    $order['intervalTyp'] = $property['value'];
                    break;

                case OrderPropertyType::INTERVAL_VALUE:
                    $order['intervalValue'] = $property['value'];
                    break;

                case OrderPropertyType::SIZE:
                    $order['size'] = $property['value'];
                    break;

                case OrderPropertyType::LOCATION_RESERVED:
                    $order['locationReserved'] = $property['value'];
                    break;
            }
        }

        $resultOrderReferences = [];
        foreach ($order['orderReferences'] as $orderReference) {
            $originOrderId = $orderReference['originOrderId'];
            $referenceOrderId = $orderReference['referenceOrderId'];

            $this->transformOrder($this->orderRepo->findOrderById($originOrderId)->toArray(), $resultOrders, $withDocumentContents);
            $this->transformOrder($this->orderRepo->findOrderById($referenceOrderId)->toArray(), $resultOrders, $withDocumentContents);

            $filter = [
                'orderId' => $originOrderId,
                'withContent' => $withDocumentContents
            ];
            /** @var DocumentRepositoryContract $repo */
            $repo = pluginApp(DocumentRepositoryContract::class);
            $repo->setFilters($filter);
            /** @var Document[] $documents */
            $documents = $repo->find()->getResult();

            $orderReference['originOrderDocuments'] = $documents;

            $filter = [
                'orderId' => $referenceOrderId,
                'withContent' => $withDocumentContents
            ];
            /** @var DocumentRepositoryContract $repo */
            $repo = pluginApp(DocumentRepositoryContract::class);
            $repo->setFilters($filter);
            /** @var Document[] $documents */
            $documents = $repo->find()->getResult();

            $orderReference['referenceOrderDocuments'] = $documents;
            $resultOrderReferences[] = $orderReference;
        }
        $order['orderReferences'] = $resultOrderReferences;

        $resultOrders[] = $order;
    }
}