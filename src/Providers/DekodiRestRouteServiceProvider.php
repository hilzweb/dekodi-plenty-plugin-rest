<?php
namespace DekodiRest\Providers;

use Plenty\Plugin\RouteServiceProvider;
use Plenty\Plugin\Routing\ApiRouter;
use Plenty\Plugin\Routing\Router;

/**
 * Class HelloWorldRouteServiceProvider
 * @package HelloWorld\Providers
 */
class DekodiRestRouteServiceProvider extends RouteServiceProvider
{
	/**
	 * @param ApiRouter $api
	 */
	public function map(ApiRouter $api)
	{
        $api->version(['v1'], ['middleware' => ['oauth']], function ($api) {
            /** Actions */
            $api->get('dekodi/orders', [
                'uses'       => 'DekodiRest\Controllers\OrdersController@all'
            ]);
        });
	}

}
